<!DOCTYPE html>
<html>
    <head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
		<meta name="description" content="Tired of giving directions? Open the app, click the contact, and we'll send them the directions. Super easy!">
        <title>iamhe.re</title>
        <link rel="shortcut icon" href="images/favicon.ico" />
        <link rel="stylesheet" href="style/jquery.mobile-1.1.0.min.css" />
        <link rel="stylesheet" href="style/my.css" />
        
        <!-- ios app install stull -->			
			<link rel="apple-touch-icon" href="/images/thum128.png"/>
			<!-- need 320x460 image <link rel="apple-touch-startup-image" href="img/splash.png" /> -->
			<!-- full screen  <meta name="apple-mobile-web-app-capable" content="yes" /> -->
        <!-- / -->
        
		<link rel="stylesheet" href="js/leaflet/leaflet.css" />
		<!--[if lte IE 8]>
			 <link rel="stylesheet" href="js/leaflet/leaflet.ie.css" />
		 <![endif]-->
		<script src="js/jquery-1.8.0.min.js"></script>
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>
        <script src="js/my.js"></script>
		
		<script type="text/javascript">
			  var _gaq = _gaq || [];
			  _gaq.push(['_setAccount', 'UA-24842418-4']);
			  _gaq.push(['_trackPageview']);

			  (function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
		</script>
		<script>
			var SITE = "<?= SITE ?>";
		</script>
    </head>
    <body>
        <!-- Home -->
        <div data-role="page" id="page1">
           <div class="jqm-version">It's Free!</div>
                <div style="text-align: center">
                    <img src="images/header.png" alt="iamhe.re"/><br/>               
					<div style="font-weight:bold;">    let your friends find you and the places you like.</div>
				</div>
				<div data-role="content" style="padding: 15px; text-align:left;display:inline-block">
					<p>
						<img src="images/star_small.png" width="24" height="24" alt="star" style="vertical-align:bottom"/>Tired of giving directions? Open the app, click the contact, and we'll send them the directions. Super easy!<br/>
						<br>
						<div style="font-weight:bold;">I'm still not sure. What's it really good for?</div>
						<UL>
							<LI>	To tell someone where you are, 
							<LI>	Meeting friends, 
							<LI>	A client or customer is visiting you, 
							<LI>	Meeting for a date, 
							<LI>	Picking someone up in your car, 
							<LI>	Getting something delivered.
							<LI>	Someone viewing your house/property
							<LI>	Telling a friend about places you have been to (like a restaurant, shop, that sort of thing)
						</UL>
						<br/>
						<div style="font-weight:bold;">how's it work?</div>
						Open <span class=logo><span style="color:#F0811A">i</span><span style="color:#FCCF1B">am</span><span style="color:#F0811A">he</span><span style="color:#FCCF1B">.</span><span style="color:#F0811A">re</span></span>, 
						select a contact and send them a message with your location or the location of one of the places you have been too.
						<br>
						They will receive an SMS message with the exact directions on how to get there.
						<br>
						Thats it! Super easy, so what are you waiting for?
               	  </p>
            </div>
			<div style="display:inline-block; vertical-align: top;padding: 15px; "><img src="images/connected.png" alt="people"   style="max-width:230px;max-height:256px"/></div>
			
            <div data-theme="a" data-role="footer" data-position="fixed">
                <div data-role="navbar" data-iconpos="top">
                    <ul>
                        <li>
                            <a href="market://details?id=com.mmtechco.iamhere" data-theme="" data-icon="">
                            <img style="display:inline-block;" src="images/play.png" alt="Get on Android "/>
                            </a>
                        </li>
                        <li>
                            <a href="http://itunes.apple.com/us/app/iamhe.re/id529147898" data-theme="" data-icon="">
                              <img style="display:inline-block;" src="images/store2.png" alt="Get on iPhone "/> 
                            </a>
                        </li>
                        <li>
							<a href="web_app" data-transition="slide" data-prefetch="false" data-dom-cache="false" data-ajax="false">
								<img style="display:inline-block;" src="images/web.png" alt="Use the web version"/> 
							</a>
                        </li>
                    </ul>
                </div>
                <h3>
					<table border="0" width=100%>
					  <tr>
						<th width=33%>&nbsp;</th>
						<th align="center" width=33%><a href="http://www.livemobileco.com"><img src="images/lm_logo.png"/></a></th>
						<th style="color:orangered" align="right" width=33%><a href="mailto:don.corbett@mmtechco.com?Subject=iamhe.re - events and branded">event | branded</th>
					  </tr>
					</table>
                </h3>
            </div>
        </div>
    </body>
</html>
