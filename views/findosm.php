<!DOCTYPE html> 
<html> 
<head> 
	<title>iamhe.re</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

	<link rel="shortcut icon" href="images/favicon.ico" />
	<script src="js/browserdetect.js"></script>
	<link rel="stylesheet" href="style/findosm.css">
	<link rel="stylesheet" href="js/leaflet/leaflet.css" />
	 <!--[if lte IE 8]>
		 <link rel="stylesheet" href="js/leaflet/leaflet.ie.css" />
	<![endif]-->
 
	 <!-- ios app install stull -->			
		<link rel="apple-touch-icon" href="/images/thum128.png"/>
		<!-- need 320x460 image <link rel="apple-touch-startup-image" href="img/splash.png" /> -->
		<!-- full screen  <meta name="apple-mobile-web-app-capable" content="yes" /> -->
	<!-- / -->
 
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24842418-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<script src="js/jquery-1.8.0.min.js"></script>
<script src="js/leaflet/leaflet.js"></script>
<script src="js/findosm.js"></script>

</head>
<body>
	<div id="findmap"></div>
</body> 
<script>
$(document).ready(function(){
	initialisemap(<?= $lat.','.$lon ?>);
});
</script>
</html> 
