<?php
	/*$value = substr(strstr($_SERVER["REQUEST_URI"], '?')//REMOVE ADDRESS
													,1);//REMOVE QUESTION MARK
	*/
	$value = $findrequest;

	$latlog	= explode(',', $value);
	
	if(3 != count($latlog) && 2 != count($latlog)
	|| false == is_numeric($latlog[0]) 
	|| false == is_float((float)$latlog[0])
	|| false == is_numeric($latlog[1]) 
	|| false == is_float((float)$latlog[1]))
	{die('');}
//	{	die($language->getLanText(144));	} // sorry bad url
	
	$dirType = 'DRIVING';
	if(3 == count($latlog))
	if('walk' == $latlog[2])
	{	$dirType = 'WALKING';	}
	
	$virial = TRUE;
	$android = FALSE;
	
	$bannerLinkUrl = 'http://www.iamhe.re';
	$bannerImage = './images/banner_en.png';
	$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
	$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
	if('zh' == $lang)
	{	$bannerImage = './images/banner_zh.png';	}
	
	if(stripos($ua,'android') !== false) 
	{	
		if('zh' == $lang)
		{	$bannerLinkUrl = 'zhelizheli.com';	}
		else
		{	$bannerLinkUrl = 'https://market.android.com/details?id=com.mmtechco.iamhere';	}	
	}	
?>
<!DOCTYPE html> 
<html> 
<head> 
<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/> 
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 
<title><?php echo $_SERVER['HTTP_HOST'];?></title>     
<link rel="stylesheet" href="style/jquery.mobile-1.1.0.min.css" />
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-24842418-5']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>

<link href="http://code.google.com/apis/maps/documentation/javascript/examples/default.css" rel="stylesheet" type="text/css" /> 
<link href="style/findStyle.css" rel="stylesheet" type="text/css" /> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&language=<?php echo $lang;?>"></script> 
<script type="text/javascript" src="http://code.google.com/apis/gears/gears_init.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript"> 
function initialize() 
{
	var directionDisplay;
	var directionsService = new google.maps.DirectionsService();
	var map;
	var child;
	var parent;
  	var markChild;
	var markParent;
	var supportGeoFlag = false;
	var markIcon = "images/mapMarker.png";
	
    directionsDisplay = new google.maps.DirectionsRenderer();
	child = new google.maps.LatLng(<?php echo $latlog[0].', '.$latlog[1]?>);

    var myOptions = 
	{
		zoom:7,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: child
    };
    
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    
	if(navigator.geolocation) 
	{	
		navigator.geolocation.getCurrentPosition(function(position) 
		{
			supportGeoFlag = true;
			parent = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);//alert(parent);
		});		
	}
	else if(google.gears)
	{		
	    geo.getCurrentPosition(function(position) 
	    {
		    supportGeoFlag = true;
	        parent = new google.maps.LatLng(position.latitude,position.longitude);
	    });	
	}

	directionsDisplay.setMap(map);
	setTimeout(setDestin, 2000);

	function setDestin()
	{
		
		if(supportGeoFlag)
		{
			var request = 
			{
			origin: parent, 
			destination: child,
			travelMode: google.maps.DirectionsTravelMode.<?php echo $dirType; ?>
			};
			
			directionsService.route(request, function(response, status) 
			{
				if (status == google.maps.DirectionsStatus.OK) 
				{	directionsDisplay.setDirections(response);	}
			});

			markParent = new google.maps.Marker({
				position: parent,
				map: map,
				icon: markIcon,
				zIndex: 999999
		    });
		}
		else
		{
			map.setCenter(child);
			map.setZoom(14);
		}

	    markChild = new google.maps.Marker({
			position: child,
			map: map,
			icon: markIcon,
			zIndex: 999999
	    });

		markChild.setAnimation(google.maps.Animation.BOUNCE);
		//infoWindowChild.open(map, markChild);
		
		if(supportGeoFlag)
		{
			//infoWindowParent.open(map, markParent);
		  	markParent.setAnimation(google.maps.Animation.DROP);
		  	//infoWindowParent.open(map, markParent);
		}
	}	
}
</script> 
</head> 
<body onload="initialize()"> 
<div  style="width: 100%; height: 100%;">
<div id="map_canvas" style="width: 100%; height: 100%;"></div>
<img id="car" class="carPer" src="images/car.png" alt="mobileminder"></img>
<img id="walk" class="carPer"src="images/walk.png" alt="mobileminder"></img>
<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#car").click(function()
	{
		window.location = "<?php echo  $_SERVER['PHP_SELF'].'?'.$latlog[0].','.$latlog[1]; ?>,car"; 
	});
	
	$("#walk").click(function()
	{
		window.location = "<?php echo  $_SERVER['PHP_SELF'].'?'.$latlog[0].','.$latlog[1]; ?>,walk";
	});
		
	if('DRIVING' == '<?php echo $dirType; ?>')
	{
		var car = document.getElementById("car");
		var walk = document.getElementById("walk");

		car.style.display="none";
		walk.style.display="block";
	}
	else
	{
		var car = document.getElementById("car");
		var walk = document.getElementById("walk");

		car.style.display="block";
		walk.style.display="none";	
	}
});
//-->
</script>
</body> 
</html> 
