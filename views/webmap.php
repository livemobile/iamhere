<!DOCTYPE html>
<html>
    <head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="description" content="Tired of giving directions? Open the app, click the contact, and we'll send them the directions. Super easy!">
        <title>iamhe.re</title>
        <link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="image_src" href="http://mobileminder.com/images/thum128.png" />
        <link rel="stylesheet" href="style/jquery.mobile-1.1.0.min.css" />
        <link rel="stylesheet" href="style/my.css" />
        
        <!-- ios app install stull -->			
			<link rel="apple-touch-icon" href="/images/thum128.png"/>
			<!-- need 320x460 image <link rel="apple-touch-startup-image" href="img/splash.png" /> -->
			<meta name="apple-mobile-web-app-capable" content="yes" />
			
			<meta name = "viewport" content = "width = device-width">
        <!-- / -->
   
	<link rel="stylesheet" href="js/leaflet/leaflet.css" />
	<!--[if lte IE 8]>
		 <link rel="stylesheet" href="js/leaflet/leaflet.ie.css" />
	 <![endif]-->
	<script src="js/jquery-1.8.0.min.js"></script>
	<script src="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>
	<script src="js/my.js"></script>
				
	<script src="js/leaflet/leaflet.js"></script>
	<script src="js/geohash/geohash.js"></script>
	<script src="js/browserdetect.js"></script>
	<script src="js/homeosm.js"></script>
	<script src="js/jquery.qrcode.min.js"></script>
	<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
	<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-24842418-4']);
		  _gaq.push(['_trackPageview']);
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
	</script>
	<script>
		var SITE = "<?= SITE ?>";
	</script>
    </head>
    <body>
        <!-- web app -->
        <div data-role="page" id="webmap">
			<div id="qroverlaywrapper" onclick="$(this).hide();">
				<div id='qroverlay'></div>
			</div>
			<div data-role="header" class='header'>
                    <div id="webappLogo" class="webapphead">
                        <!--
                        <a href="<?= 'http://'.SITE ?>" class=logo><span style="color:#F0811A">i</span><span style="color:#FCCF1B">am</span><span style="color:#F0811A">he</span><span style="color:#FCCF1B">.</span><span style="color:#F0811A">re</span></a>
						-->
						<a href="<?= 'http://'.SITE ?>"><img src="images/header.png" alt="iamhere" style="max-width: 100%"/></a>
                    </div>
                    <div id="mapLink" class="webapphead">
						<div id="shareLinksWrapper">
							<div id="showqr" class="shareIcon" onclick="genAndDisplayQR();"></div>
							<div id="maillink" class="shareIcon" onclick="genOpenMailLink();"></div>
							<div id="twitLink" class="shareIcon" onclick="genAndOpenTwitter();"></div>
						</div>
						<a id="findlink"></a>
                    </div>
			</div>
			<div data-role="content" class="content">
				<div id="homeMap"></div>
			</div>
			<div data-theme="a" data-role="footer" class='footer' data-position="fixed">
                <h3>
					<table border="0" width=100%>
					  <tr>
						<th width=33%>&nbsp;</th>
						<th align="center" width=33%><a href="http://www.livemobileco.com"><img src="images/lm_logo.png"/></a></th>
						<th style="color:orangered" align="right" width=33%><a href="mailto:don.corbett@mmtechco.com?Subject=iamhe.re - events and branded">event | branded</th>
					  </tr>
					</table>
                </h3>
            </div>
		</div>
</html>
