<?php
/*
 * Class responcible for all decoding and database logging.
 */

class Reqlogger
{
	private $username = 'iamhereAdmindb';
	private $password = 'BP9fELBf';
	private $db_url = 'mysql';
	private $dbname = 'iamheredb';
	private $pdo;
	
	private $numChars;
	
	public function __construct()
	{
		session_start();
		$datasource = 'mysql:dbname='.$this->dbname.';host='.$this->db_url.';charset=UTF8';
		$this->pdo = new PDO($datasource, $this->username, $this->password, array(PDO::ATTR_PERSISTENT => true));
	}
	
	private function addWebSenderLogEntry($hash, $lat, $lon, $os, $browser, $genDate)
	{
		$query = 'INSERT INTO '.REQTBL.'
		(
			hash,
			servertime,
			slat,
			slon,
			splatform,
			sbrowser,
			s_os,
			sdate
		)
		VALUES
		(
			:hash,
			:servertime,
			:slat,
			:slon,
			:splatform,
			:sbrowser,
			:s_os,
			:sdate
		)';
		
		$sth = $this->pdo->prepare($query);
		$sth->bindValue(':hash', $hash);
		$sth->bindValue(':servertime', time());		
		$sth->bindValue(':slat', $lat);
		$sth->bindValue(':slon', $lon);		
		$sth->bindValue(':splatform', 'Browser');
		$sth->bindValue(':sbrowser', $browser);
		$sth->bindValue(':s_os', $os);
		$sth->bindValue(':sdate', $genDate);
		$sth->execute();
		//var_dump($sth->errorInfo());
	}
	
	/*
	 * should decode and log the loggin characters and return the number of logging characters so that they can be stripped off the geohash
	 */
	public function decodeAndLog($request)
	{
		$this->numChars = 0;
		
		if($this->isDevice($request[0]))
		{
			$this->numChars = 5; //25 bits
			$geohash = substr($request, $this->numChars, strlen($request) - $this->numChars);
			$this->logDevice($this->getLogBin($request, $this->numChars));
		}
		else // web app
		{
			$this->numChars = 4; //20 bits
			$this->logWebApp($request);
		}		
		return $this->numChars;
	}
	
	private function logDevice($logBinArray)
	{
		//TODO: logDevice
	}
	
	private function logWebApp($request)
	{
		$logBin = $this->getLogBin($request);
		
		$platformBits = substr($logBin, 0, 4);
		$browserBits = substr($logBin, 4, 4);
		$creationBits = substr($logBin, 8, 12);
		
		$geohash = substr($request, $this->numChars, strlen($request) - $this->numChars);
		$geo =  new GeoHash();
		$geo->setHash($geohash);		
		$lat = $geo->getLatitude();
		$lon = $geo->getLongitude();
		
		switch ($platformBits)
		{
			case '1110':
				$platform = 'Linux';
				break;
			case '1100':
				$platform = 'Windows';
				break;
			case '1011':
				$platform = 'Mac';
				break;
			case '1010':
				$platform = 'iOS';
				break;
			case '1001':
				$platform = 'Android';
				break;
			case '1111':
				$platform = 'Undetermined';
				break;
			default:
				$platform = 'Error, unknown bit string';
				break;
		}
		
		switch($browserBits)
		{
			case '0001':
				$browser = 'Firefox';
				break;
			case '0010':
				$browser = 'Chrome';
				break;
			case '0100':
				$browser = 'Internet Explorer';
				break;
			case '0011':
				$browser = 'Opera';
				break;
			case '0101';
				$browser = 'Safari';
				break;
			case '0110';
				$browser = 'Android Browser';
				break;
			default:
				$browser = 'Undetermined';
				break;
		}
		
		$epoch = new DateTime('2012-07-01');
		$dateDiff = base_convert($creationBits, 2, 10); // time from epoch in days
		$genDate = $epoch->add(new DateInterval('P'.$dateDiff.'D'));
		
		//sender data
		$hash = hash("sha256", $request.microtime()); //TODO: this may not be the best solution if there are a lot of requests could get a colision.
		$lat;
		$lon;
		$platform;
		$browser;
		$dateDiff;
		$genDate = $genDate->format('Ymd');
		
		$_SESSION['hash'] = $hash;
		$this->addWebSenderLogEntry($hash, $lat, $lon, $platform, $browser, $genDate);
	}
	
	/*
	 * converts the base 32 log character string to a binary one.
	 */
	private function getLogBin($logChars)
	{
		$logBin = '';
		for($i = 0; $i < $this->numChars; $i++)
		{
			$logBin .= $this->charToBin($logChars[$i]);
		}
		return $logBin;
	}
	
	private function isDevice($firstChar)
	{
		$platformBits = $this->charToBin($firstChar);
		return ($platformBits[0] == '0') ? TRUE : FALSE;
	}
	
	/*
	 * Converts from base 32 string to base 2 string with leading zeros
	 */
	private function charToBin($logChar)
	{
		return sprintf('%05b', base_convert($logChar, 32, 10));
	}
	
	/*
	 * @brief
	 * 	to log the receives data to the database
	 */
	function logRecieversData($hash, $os, $browser, $lat, $lon)
	{
		$query = 'UPDATE '.REQTBL.'
					SET rplatform = :rplatform,
						rbrowser = :rbrowser,
						rlat = :rlat,
						rlon = :rlon
					WHERE hash = :hash';
		
		$sth = $this->pdo->prepare($query);
		$sth->bindValue(':rplatform', $os);
		$sth->bindValue(':rbrowser', $browser);		
		$sth->bindValue(':rlat', $lat);
		$sth->bindValue(':rlon', $lon);		
		$sth->bindValue(':hash', $hash);
		$result = $sth->execute();
		
		return $result;
	}
}
