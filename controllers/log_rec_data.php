<?php
	/*
	 * Used to log the link recievers info. 
	 * Should be called by ajax in RecLogData javaScript function in findosm.js
	 */
	
	$hash = $_SESSION['hash'];
	
	if(empty($hash))
	{
		$result = 'no hash found';
	}
	else
	{
		$os = $_POST['os'];
		$browser = $_POST['browser'];
		$lat = $_POST['lat'];
		$lon = $_POST['lon'];
		
		if(empty($os) || empty($browser) || empty($lat) || empty($lon))
		{
			$result = 'missing post data';
		}
		else
		{
			if($reqLog->logRecieversData($hash, $os, $browser, $lat, $lon))
			{	$result = 'Success';	}
			else
			{	$result = 'Problem writing to the database';	}
		}
	}
	
	header('Content-Type: application/json');
	$data = json_encode(array('result' => $result));
	echo $data;
?>
