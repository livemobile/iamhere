var map,recMarker, sendMarker;

function initialisemap(slat, slon)
{
	var mzoom = 16;	
	map = L.map('findmap');
	
	var targetIcon = L.icon({
		iconUrl: 'images/target.png',
		iconSize: [64, 64],
		iconAnchor: [32, 64]
	});
	
	map.setView([slat, slon], mzoom);
	
	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { //osm's tile servers
	//L.tileLayer('http://maps.livemobileco.com/osm/{z}/{x}/{y}.png', { // our own tile server
		maxZoom: 18
	}).addTo(map);
	
	sendMarker = L.marker([slat, slon], {icon: targetIcon}).addTo(map);
	
	getLocation();
}

function getLocation()
{
	//asynchronously calls after the users gotPostion after the users allows their location to be shared
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function (pos) {gotPostion(pos)} );
	}
}

function gotPostion(pos)
{	
	var lat = pos.coords.latitude, 
		lon = pos.coords.longitude;
		
	var sourceIcon = L.icon({
		iconUrl: 'images/source.png',
		iconSize: [64, 64],
		iconAnchor: [32, 64]
	});

	var recMarker = L.marker([lat, lon], {icon: sourceIcon}).addTo(map);
	
	var bounds = new L.LatLngBounds(sendMarker.getLatLng(), recMarker.getLatLng());
	drawWay(sendMarker.getLatLng(), recMarker.getLatLng());

	map.fitBounds(bounds); // figures out optimum zoom and map centering based on the points
	RecLogData(lat, lon);
}

//draw a line between each of the points in the route
function drawWay(sendLatLng,recLatLng)
{
	var points =  getRoute(sendLatLng.lat,sendLatLng.lng,recLatLng.lat,recLatLng.lng,"motorcar","1");
	var pointsL = [];			
	for (i=0; i < points.length; i++)
	{
		pointsL[i]= new L.LatLng(points[i][1],points[i][0]);
	}			
	var polyline = L.polyline(pointsL,{color: '#2489CE', opacity: 0.8}).addTo(map);
}

//gets the routing point between the two points. Currently uses a third party service.
function getRoute(flat,flon,tlat,tlon,vehicle,fast)
{
	var getjson;
	$.ajax({
		url:"../routing.php",
		data:{
			flat:flat,
			flon:flon,
			tlat:tlat,
			tlon:tlon,
			v:vehicle,
			fast:fast
		},
		cache:false,
		dataType:"json",
		async:false,
		success:function(data){
			getjson = data.coordinates;
		}
	});
	return getjson;
}

function RecLogData(lat, lon)
{
	 $.ajax({
            url :  'iamhere.local/log_rec_data.php',
            type : 'POST',
            cache : false,
            data: { 'os':window.ui.os, 'browser': window.ui.browser, 'lat': lat, 'lon': lon },
            dataType: 'json'
        });
}
