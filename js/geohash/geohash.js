var geohash = (function () {
  var geohash = {
    /**
     * Success callback used with the locate() method
     * @type function
     */
    success_callback: null,

    /**
     * Error callback used with the locate() method
     * @type function
     */
    error_callback: null,

    /**
     * Hash of decimal values keyed on base32 representation
     */
    base32_to_decimal: {
      "0": 0,
      "1": 1,
      "2": 2,
      "3": 3,
      "4": 4,
      "5": 5,
      "6": 6,
      "7": 7,
      "8": 8,
      "9": 9,
      "b": 10,
      "c": 11,
      "d": 12,
      "e": 13,
      "f": 14,
      "g": 15,
      "h": 16,
      "j": 17,
      "k": 18,
      "m": 19,
      "n": 20,
      "p": 21,
      "q": 22,
      "r": 23,
      "s": 24,
      "t": 25,
      "u": 26,
      "v": 27,
      "w": 28,
      "x": 29,
      "y": 30,
      "z": 31
    },

    /**
     * Hash of base32 values keyed on decimal representation
     */
    decimal_to_base32: {
      "0": 0,
      "1": 1,
      "2": 2,
      "3": 3,
      "4": 4,
      "5": 5,
      "6": 6,
      "7": 7,
      "8": 8,
      "9": 9,
      "10": "b",
      "11": "c",
      "12": "d",
      "13": "e",
      "14": "f",
      "15": "g",
      "16": "h",
      "17": "j",
      "18": "k",
      "19": "m",
      "20": "n",
      "21": "p",
      "22": "q",
      "23": "r",
      "24": "s",
      "25": "t",
      "26": "u",
      "27": "v",
      "28": "w",
      "29": "x",
      "30": "y",
      "31": "z"
    },

    /**
     * Uses the browsers geolocation capabilities to determine the user's location
     * 
     * @param {function} [success] Callback used when the location was successfully determined, will receive the position object from navigator.geolocation.getCurrentPosition with the added attribute hash.
     * @param {function} [error]
     */
    locate: function (success, error) {
      geohash.success_callback = success;
      geohash.error_callback = error;

      navigator.geolocation.getCurrentPosition(geohash.success, geohash.error);
    },

    /**
     * Callback for navigator.geolocation.getCurrentPosition after successful position determination, will call the callback supplied to locate().
     * 
     * @param {object} position
     */
    success: function (position) {
      position.coords.hash = geohash.hash(position.coords.latitude, position.coords.longitude);

      if (geohash.success_callback != undefined) {
        geohash.success_callback(position);
        geohash.success_callback = null;
      }
    },

    /**
     * Callback for navigator.geolocation.getCurrentPosition after an error has occurred, will call the callback supplied to locate().
     */
    error: function () {
      if (geohash.error_callback != undefined) {
        geohash.error_callback();
        geohash.error_callback = null;
      }
    },

    /**
     * Hashes the provided latitude and longitude to the precision specified.
     * 
     * @param {Number} latitude Latitude of the coordinate you want to hash
     * @param {Number} longitude Longitude of the coordinate you want to hash
     * @param {Number} [precision] Number of bits to use when spcifying accuracy of a coordinate
     * 
     * @returns {String} Hash of the given coordinate to the precision specified (or 20 bits if not)
     */
    hash: function (latitude, longitude, precision) {
      precision = precision == undefined ? 20 : precision;

      // Convert the coords to binary
      var lat_bin = geohash.latitude_to_binary(latitude, precision);
      var lng_bin = geohash.longitude_to_binary(longitude, precision);
      var bin_hash = "";

      // Generate the binary hash
      for (var i = 0; i < precision; i++) {
        bin_hash = bin_hash + lng_bin[i] + lat_bin[i];
      }

      // Chunk the hash into blocks of 5 bits (5 bits = dec 0-31)
      var chunk_size = 5;
      var bin_chunks = [];
      var dec_chunks = [];
      var b32_chunks = [];

      for (var i = 0; i < bin_hash.length; i += chunk_size) {
        bin_chunks.push(bin_hash.slice(i, i + chunk_size));
      }

      // Convert the binary chunks into decimal
      for (var i = 0; i < bin_chunks.length; i++) {
        dec_chunks.push(geohash.base(bin_chunks[i], 10, 2));
      }

      // Convert the decimal chunks to base 32
      for (var i = 0; i < dec_chunks.length; i++) {
        b32_chunks.push(geohash.decimal_to_base32[dec_chunks[i]]);
      }

      // Join the chunks array
      return b32_chunks.join("")
    },

    /**
     * Converts the provided latitude into binary using the number of bits specified in precision. 
     * @see This does not use base conversion please see the <a href="http://en.wikipedia.org/wiki/Geohash">Geohash documentation</a>
     * 
     * @param {Number} latitude The latitude coordinate to convert to binary
     * @param {Number} precision The number of bits to convert to
     * 
     * @return {String} Binary representation of the latitude at precision-length bits
     */
    latitude_to_binary: function (latitude, precision) {
      var bounds = {
        max: 90,
        min: -90
      };

      return geohash.to_binary(latitude, precision, bounds);
    },

    /**
     * Converts the provided longitude into binary using the number of bits specified in precision. 
     * @see This does not use base conversion please see the <a href="http://en.wikipedia.org/wiki/Geohash">Geohash documentation</a>
     * 
     * @param {Number} longitude The longitude coordinate to convert to binary
     * @param {Number} precision The number of bits to convert to
     * 
     * @return {String} Binary representation of the longitude at precision-length bits
     */
    longitude_to_binary: function (longitude, precision) {
      var bounds = {
        max: 180,
        min: -180
      };

      return geohash.to_binary(longitude, precision, bounds);
    },

    /**
     * Converts the provided coordinate to binary using the bounds specified until the precision number bits is met.
     * @see This does not use base conversion please see the <a href="http://en.wikipedia.org/wiki/Geohash">Geohash documentation</a>
     * 
     * @param {Number} coord Coordinate to convert
     * @param {Number} precision Number of bits to use
     * @param {Object} bounds provides the maximum and minimum bounds to check against
     * 
     * @return {String} Binary representation of the coordinate at precision-length bits
     */
    to_binary: function (coord, precision, bounds) {
      var mid = (bounds.max + bounds.min) / 2;
      var new_bounds = {};
      var bit = "";

      if (precision == 0) {
        return bit;
      }
      else {
        if (coord > mid) {
          new_bounds.max = bounds.max;
          new_bounds.min = mid;
          bit = "1";
        }
        else {
          new_bounds.max = mid;
          new_bounds.min = bounds.min;
          bit = "0";
        }

        return bit + geohash.to_binary(coord, precision - 1, new_bounds);
      }
    },

    /**
     * Converts the provided hash into latitude and longitude coordinates
     * 
     * @param {String} hash Represents a latitude and longitude in geohash form. The longer the has the more accurate the coordinates
     *
     * @return {Object} Object containing a latitude and longitude key
     */
    dehash: function (hash) {
      var dec_chunks = [];
      var bin_chunks = [];

      // Convert the base32 string into decimal chunks
      for (var i = 0; i < hash.length; i++) {
        dec_chunks.push(geohash.base32_to_decimal[hash[i]]);
      }

      // Convert the decimal chunks into binary chunks
      for (var i = 0; i < dec_chunks.length; i++) {
        var chunk = geohash.base(dec_chunks[i], 2, 10);

        while (chunk.length < 5) {
          chunk = "0" + chunk;
        }

        bin_chunks.push(chunk);
      }

      // Combine the binary chunks
      var bin_hash = bin_chunks.join('');

      // Generate the binary strings for the latitude and longitude
      var lat_bin = "";
      var lng_bin = "";

      for (var i = 0; i < bin_hash.length; i++) {
        if (i % 2 == 1) {
          lat_bin += bin_hash[i];
        }
        else {
          lng_bin += bin_hash[i];
        }
      }

      // Determine the position based of the bounds calculations from before
      var coords = {
        latitude: geohash.latitude_from_binary(lat_bin),
        longitude: geohash.longitude_from_binary(lng_bin)
      }

      return coords;
    },

    /**
     * Converts the given latitude binary into a umber representation of the position
     * @see This does not use base conversion please see the <a href="http://en.wikipedia.org/wiki/Geohash">Geohash documentation</a>
     * 
     * @param {String} latitude_binary Binary string representing a latitude value
     * 
     * @return {Number} The binary translated into a usable latitude coordinate
     */
    latitude_from_binary: function (latitude_binary) {
      var bounds = {
        max: 90,
        min: -90
      };

      return geohash.from_binary(latitude_binary, bounds);
    },

    /**
     * Converts the given longitude binary into a number representation of the position
     * @see This does not use base conversion please see the <a href="http://en.wikipedia.org/wiki/Geohash">Geohash documentation</a>
     * 
     * @param {String} longitude_binary Binary string representing a longitude value
     * 
     * @return {Number} The binary translated into a usable longitude coordinate
     */
    longitude_from_binary: function (longitude_binary) {
      var bounds = {
        max: 180,
        min: -180
      };

      return geohash.from_binary(longitude_binary, bounds);
    },

    /**
     * Converts the provided binary into a coordinate number using the bounds specified
     * @see This does not use base conversion please see the <a href="http://en.wikipedia.org/wiki/Geohash">Geohash documentation</a>
     * 
     * @param {String} binary Binary string to convert into a coordinate
     * @param {Object} bounds provides the maximum and minimum bounds to check against
     * 
     * @return {Number} Coordinate translated from the hash
     */
    from_binary: function (binary, bounds) {
      var mid = (bounds.max + bounds.min) / 2;
      var new_bounds = {};

      if (binary.length == 0) {
        return mid;
      }
      else {
        if (binary[0] == "1") {
          new_bounds.max = bounds.max;
          new_bounds.min = mid;
        }
        else {
          new_bounds.max = mid;
          new_bounds.min = bounds.min;
        }

        return geohash.from_binary(binary.substr(1), new_bounds);
      }
    },

    /**
     * Converts a number from a given base into another
     * @see Pulled from <a href="http://www.bucabay.com/web-development/base-conversion-in-javascript/">Buca Bay Article</a>
     * 
     * @param {Number} n Number to convert
     * @param {Number} to Base we are converting to
     * @param {Number} [from] Base we are converting from, defaults to 10
     * 
     * @returns {Number} The value converted to the appropriate base
     */
    base: function(n, to, from) {
      return parseInt(n, from || 10).toString(to);
    }
  };
  
  return geohash;
}.call());