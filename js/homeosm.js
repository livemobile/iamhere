$(document).ready(function () { initialiseHomeMap(); });

var homemap, marker, micon, link, logChars;

function initialiseHomeMap()
{
	var mzoom = 2; //initial zoom level
	var startpos = [0, 0]; //initial start position
	
	homemap = L.map('homeMap', {doubleClickZoom: true});
	
	homemap.setView(startpos, mzoom);
	
	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	//L.tileLayer('http://maps.livemobileco.com/osm/{z}/{x}/{y}.png', {
		maxZoom: 18
	}).addTo(homemap);

	micon = L.icon({
		iconUrl: 'images/target.png',
		iconSize: [64, 64],
		iconAnchor: [32, 64]
	});
	marker = L.marker(startpos, {icon: micon, draggable: false}).addTo(homemap);
	
	homemap.addEventListener('move', function() {
		handleMarkerMoved();
		marker.setLatLng(homemap.getCenter());
	});
	
	logChars = genLogChars();
	getLocation();
}

function genAndDisplayQR()
{
	$('#qroverlay').empty();
	$('#qroverlay').qrcode(link);
	$('#qroverlaywrapper').show();
}

function handleMarkerMoved()
{
	generateLink();
}

function handleDoubleClick(e)
{
	marker.setLatLng(e.latlng);
	handleMarkerMoved();
}

function generateLink()
{
	var precision = 24; //level of precision used when generating the geohash. Will determine the length of the hash

	var hash = geohash.hash(homemap.getCenter().lat, homemap.getCenter().lng, precision);
	//add logging characters
	hash = logChars + hash;
	link = SITE + '/' + hash;
	$('#findlink').attr('href','http://'+link).attr('target', '_blank').text(link); // link
	
	$('#maillink').attr('href','mailto:?subject=Iamhere&body='+'Location: http://'+link).attr('target', '_blank');
}

function getLocation()
{
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function (pos) {gotPostion(pos)} );
	}
}

function gotPostion(pos)
{	
	var lat = pos.coords.latitude, 
		lon = pos.coords.longitude;

	homemap.setView([lat, lon], 14);
	generateLink();
}

function genAndOpenTwitter()
{
	var twitLink = 'http://www.twitter.com/intent/tweet?&text=Location:+' + link;
	window.open(twitLink, '_blank');
}

function genOpenMailLink()
{
	var mailLink = 'mailto:?subject=Iamhere&body=Location: http://' + link;
	window.open(mailLink, '_blank');
}

function genLogChars()
{
	var platformBits, browserBits, creationBits, endBits, logChars = '';
	var epoch = new Date(2012, 7, 1);
	var now = new Date();
	var dateDiff = now - epoch; // miliseconds since the epoch
	
	switch(window.ui.os)
	{
		case 'Linux':
			platformBits = '1110';
			break;
		case 'Windows 32 bit':
		case 'Windows 64 bit':
		case 'Windows':
			platformBits = '1100';
			break;
		case 'Mac':
			platformBits = '1011';
			break;
		case 'iOS':
			platformBits = '1010';
			break;
		case 'Android':
			platformBits = '1001';
			break;
		default:
			platformBits = '1111';
			break;
	}

	switch(window.ui.browser)
	{
		case 'Firefox':
			browserBits = '0001';
			break;
		case 'Chrome':
			browserBits = '0010';
			break;
		case 'Internet Explorer':
			browserBits = '0100';
			break;
		case 'Opera':
			browserBits = '0011';
			break;
		case 'Safari':
			browserBits = '0101';
			break;
		case 'Android Webkit Browser':
			browserBits = '0110';
			break;
		default:
			browserBits = '1111';
			break;
	}

	dateDiff = parseInt(dateDiff/86400000);	//milliseconds to days

	creationBits = dateDiff.toString(2); //convert the decimal datediff value to a binary string
	
	//need to add the correct number of leading zeros to get the right number of bits
	for( ; creationBits.length < 12; creationBits = '0' + creationBits);
	
	endBits = platformBits + browserBits + creationBits;
	
	//take every 5 bits and convert it to a base 32 chartacter, addin it to the logChars 
	for(var i = 0, tmpBits; i < endBits.length; i += 5)
	{
		tmpBits = '';
		
		for(var j = 0; j < 5; j++)
		{	tmpBits += endBits[i+j];	}
		
		tmpBits = parseInt(tmpBits, 2);
		logChars += tmpBits.toString(32);
	}
	
	return logChars;
}

function genZerosString(num)
{
	var zerosString = '';
	for(var i = 0; i < num; i++)
	{
		zerosString += '0'
	}
}

// TODO: THIS ISN'T WORKING RIGHT ON IOS
(function() {
	var fixgeometry = function() {
		/* Some orientation changes leave the scroll position at something
		* that isn't 0,0. This is annoying for user experience. */
		scroll(0, 0);

		/* Calculate the geometry that our content area should take */
		var header = $(".header:visible");
		var footer = $(".footer:visible");
		var content = $(".content:visible");
		var viewport_height = $(window).height();

		var content_height = viewport_height - header.outerHeight() - footer.outerHeight();

		/* Trim margin/border/padding height */
		content_height -= (content.outerHeight() - content.height());
		content.height(content_height);
	  
		homemap.invalidateSize(true);
	}; /* fixgeometry */

	$(document).ready(function() {
	  $(window).bind("orientationchange resize pageshow", fixgeometry);
	});
})();
