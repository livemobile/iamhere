<?php
	define('SITE', $_SERVER['SERVER_NAME']);
	define('REQTBL', 'request');
	
	include_once 'models/requestLogger.php';
	$reqLog = new Reqlogger();
	
	if(SITE == 'iamhere.local')
	{	define('MODE', 0);	}//0: develop
	else  
	{	define('MODE', 1);	}//1: live
	
	switch (MODE)
	{
		case 0:
			define('DB_URL', 'dev');
			break;
		
		case 1:
			define('DB_URL', 'mysql');
			break;
	}
	
	$request = $_SERVER['REQUEST_URI'];
	
	if(empty($request) || $request == '/')  // default to the homepage if no request found!!!!
	{
		include_once 'views/home.php';
	}
	elseif(strpos($request, 'find.php?') !== FALSE) // if "find.php?" is found. This is to handle legacy clients.
	{	
		//legacy clients have the lat and lon in the post eg. find.php?65.2342,43.9879
		$findrequest = preg_replace('/\/find.php\?/', '', $request, 1);
		$latlog	= explode(',', $findrequest);
		
		if(count($latlog) < 2){ include_once 'views/home.php'; exit();	}
		
		$lat = $latlog[0];
		$lon = $latlog[1];
		
		include_once 'views/findosm.php';
	}
	elseif(strpos($request, 'web_app') !== FALSE)
	{
		include_once 'views/webmap.php';
	}
	elseif(strpos($request, 'log_rec_data') !== FALSE)
	{
		include_once 'controllers/log_rec_data.php';
	}
	else //TODO: VALIDATE GEOHASH AND DISPLAY 404 IF INVALID
	{
		include_once 'libraries/geohash.php';
				
		$request = preg_replace('/\//', '', $request, 1); // remove leading /
		
		$numchars = $reqLog->decodeAndLog($request);
			
		$request = substr($request, $numchars, strlen($request) - $numchars);		
		
		$geo =  new GeoHash();
		$geo->setHash($request);
		
		//get lat and long from geohash
		$lat = $geo->getLatitude();
		$lon = $geo->getLongitude();
		
		include_once 'views/findosm.php';
	}
